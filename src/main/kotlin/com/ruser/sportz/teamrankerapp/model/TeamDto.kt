package com.ruser.sportz.teamrankerapp.model

data class TeamDto(
    val teamName: String,
    val teamCode: String
){
    var wins: Int = 0
    var losses: Int = 0
    var weightedRecord: Double = 0.0
    var record: Double = 0.0
    var recordAdjustment: Double = 0.0
    var weightedRank: Int = 0
    var rank: Int = 0
    var rankAdjustment: Int = 0
}

