package com.ruser.sportz.teamrankerapp.model

import org.springframework.data.cassandra.core.mapping.Column
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.Table

@Table(value = "seasons")
data class SeasonEntity (

    @PrimaryKey
    val season: String,
    @Column(value = "description")
    val description: String

    )