package com.ruser.sportz.teamrankerapp.model

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.PrimaryKey
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn
import org.springframework.data.cassandra.core.mapping.Table
import org.springframework.data.cassandra.core.mapping.Column
import java.io.Serializable

@Table(value = "teams")
data class TeamEntity(

    @PrimaryKey
    val key: TeamPrimaryKey,
    @Column(value = "teamname")
    val teamName: String
)

@PrimaryKeyClass
data class TeamPrimaryKey(
    @PrimaryKeyColumn(name = "season", type=PrimaryKeyType.PARTITIONED)
    val season: String,
    @PrimaryKeyColumn(name = "teamcode")
    val teamCode: String
) : Serializable