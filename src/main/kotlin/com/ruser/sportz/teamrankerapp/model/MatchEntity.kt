package com.ruser.sportz.teamrankerapp.model

import org.springframework.data.cassandra.core.cql.PrimaryKeyType
import org.springframework.data.cassandra.core.mapping.*
import java.util.*

@Table(value = "matches")
data class MatchEntity (

    @PrimaryKey
    val key: MatchPrimaryKey,
    @Column(value = "winner")
    val winner: String,
    @Column(value = "loser")
    val loser: String,
    @Column(value = "notes")
    val notes: String?
)

@PrimaryKeyClass
data class MatchPrimaryKey(
    @PrimaryKeyColumn(name = "season", type = PrimaryKeyType.PARTITIONED)
    val season: String,
    @PrimaryKeyColumn(name = "id")
    val id: UUID
)