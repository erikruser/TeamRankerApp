package com.ruser.sportz.teamrankerapp.model

import java.util.UUID

data class MatchDto (
    var id: UUID?,
    val winner: String,
    val loser: String,
    val group: String?
)
