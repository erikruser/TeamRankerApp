package com.ruser.sportz.teamrankerapp.model

data class SeasonDto (
    val seasonName: String,
    val description: String
)