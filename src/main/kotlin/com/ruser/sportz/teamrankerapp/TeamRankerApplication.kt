package com.ruser.sportz.teamrankerapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TeamRankerApplication

fun main(args: Array<String>) {
	runApplication<TeamRankerApplication>(*args)
}