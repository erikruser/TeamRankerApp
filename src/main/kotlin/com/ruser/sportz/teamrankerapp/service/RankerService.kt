package com.ruser.sportz.teamrankerapp.service

import com.ruser.sportz.Ranker
import com.ruser.sportz.teamrankerapp.exceptions.ObjectNotFoundException
import com.ruser.sportz.teamrankerapp.exceptions.ResponseObject
import com.ruser.sportz.teamrankerapp.model.MatchDto
import com.ruser.sportz.teamrankerapp.model.SeasonDto
import com.ruser.sportz.teamrankerapp.model.TeamDto
import com.ruser.sportz.teamrankerapp.repo.MatchRepository
import com.ruser.sportz.teamrankerapp.repo.SeasonRepository
import com.ruser.sportz.teamrankerapp.repo.TeamRepository
import com.ruser.sportz.teamrankerapp.util.Mapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import kotlin.collections.HashMap

@Component
class RankerService (
    private val teamRepository: TeamRepository,
    private val matchRepository: MatchRepository,
    private val seasonRepository: SeasonRepository,
    private val mapper: Mapper
) {

    private val seasonRankers: HashMap<String, Ranker> = HashMap()

    @Value("\${ranker.zeroMatchesAsZero}")
    private val zeroMatchTeamsRatedZero: Boolean = false

    fun getAllTeams(season: String, sortAlpha: Boolean): List<TeamDto> {
        var ranker = seasonRankers[season]
        if(ranker == null){
            ranker = buildRankerForSeason(season)
            if(ranker == null){
                return emptyList()
            }
        }

        val teamsRankedByWeightedRecord = ranker.rankedTeams.map { t -> mapper.mapTeamLibToDto(t) }
        teamsRankedByWeightedRecord.forEachIndexed { i, team ->
            team.weightedRank = i
            team.record = team.wins.toDouble() / (team.wins.toDouble() + team.losses.toDouble())
            team.recordAdjustment = team.weightedRecord - team.record
        }
        val maxRank = teamsRankedByWeightedRecord.last().weightedRank + 1
        val teamsRankedByRecord = teamsRankedByWeightedRecord.sortedBy { it.record }
        teamsRankedByRecord.forEachIndexed { i, team ->
            team.rank = maxRank - i
            team.weightedRank = maxRank - team.weightedRank
            team.rankAdjustment = team.rank - team.weightedRank
        }

        return if(sortAlpha){
            teamsRankedByWeightedRecord.sortedBy { it.teamName }
        } else {
            teamsRankedByWeightedRecord.sortedBy { it.weightedRank }
        }
    }

    fun addTeam(season: String, teamDto: TeamDto) {
        teamRepository.save(mapper.mapTeamDtoToEntity(season, teamDto))
        //Always rebuild ranker when team is added
        buildRankerForSeason(season)
    }

    fun getTeam(season: String, teamCode: String): TeamDto {
        return getAllTeams(season, true).firstOrNull { t -> t.teamCode == teamCode }
            ?: throw ObjectNotFoundException(
                ResponseObject("Team not found.", "Team", teamCode)
            )
    }

    fun buildRankerForSeason(season: String) : Ranker? {
        val teamsForSeason = teamRepository.findByKeySeason(season)
        if(teamsForSeason.isEmpty()){
            return null
        }
        val teamsForRanker = teamsForSeason.map { t -> mapper.mapTeamEntityToLib(t) }
        val ranker = Ranker(teamsForRanker)
        ranker.zeroMatchTeamsRatedZero = zeroMatchTeamsRatedZero
        val matchesForSeason = matchRepository.findByKeySeason(season)
        matchesForSeason.forEach { matchEntity ->
            val match = mapper.mapMatchEntityToLib(ranker, matchEntity)
            if(match != null) {
                ranker.addMatch(match)
            }
        }
        seasonRankers[season] = ranker
        return ranker
    }

    fun getAllMatches(season: String): List<MatchDto> {
        return matchRepository.findByKeySeason(season)
            .map { m -> mapper.mapMatchEntityToDto(m)!! }
            .sortedBy { m -> m.group }
    }

    fun addMatch(season: String, matchDto: MatchDto) {

        val loser = teamRepository.findByKeySeasonAndKeyTeamCode(season, matchDto.loser)
        val winner = teamRepository.findByKeySeasonAndKeyTeamCode(season, matchDto.winner)
        if(loser == null || winner == null){
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }

        matchRepository.save(mapper.mapMatchDtoToEntity(season, matchDto))
        var ranker = seasonRankers[season]
        if(ranker == null){
            buildRankerForSeason(season)
        }else{
            ranker.addMatch(mapper.mapMatchDtoToLib(ranker, matchDto))
        }
    }

    fun getMatch(season: String, id: UUID): MatchDto? {
        return mapper.mapMatchEntityToDto(matchRepository.findByKeySeasonAndKeyId(season, id))
    }

    fun addSeason(season: SeasonDto) {
        seasonRepository.save(mapper.mapSeasonDtoToEntity(season))
    }

    fun getAllSeasons() : List<SeasonDto> {
        return seasonRepository.findAll().map { s -> mapper.mapSeasonEntityToDto(s) }.sortedBy { it.seasonName }
    }

    fun getReport(season: String) : String {

        var rankedTeams = getAllTeams(season, false)

        return rankedTeams.mapIndexed { index, teamDto ->
            val roundedRecord = BigDecimal(teamDto.weightedRecord).setScale(4, RoundingMode.HALF_UP)
            "${index + 1}. ${teamDto.teamName} (${teamDto.wins}, ${teamDto.losses}, $roundedRecord)"
        }.joinToString(separator = "\n")
    }
}