package com.ruser.sportz.teamrankerapp.repo

import com.ruser.sportz.teamrankerapp.model.MatchEntity
import org.springframework.data.cassandra.repository.CassandraRepository
import java.util.*

interface MatchRepository : CassandraRepository<MatchEntity, UUID> {

    fun findByKeySeason(season: String) : List<MatchEntity>

    fun findByKeySeasonAndKeyId(season: String, id: UUID): MatchEntity
}