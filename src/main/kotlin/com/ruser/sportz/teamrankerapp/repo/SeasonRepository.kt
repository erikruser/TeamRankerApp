package com.ruser.sportz.teamrankerapp.repo

import com.ruser.sportz.teamrankerapp.model.SeasonEntity
import org.springframework.data.cassandra.repository.CassandraRepository
import java.util.*

interface SeasonRepository : CassandraRepository<SeasonEntity, UUID> {
}