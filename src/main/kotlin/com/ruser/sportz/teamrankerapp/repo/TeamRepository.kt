package com.ruser.sportz.teamrankerapp.repo

import com.ruser.sportz.teamrankerapp.model.TeamEntity
import org.springframework.data.cassandra.repository.CassandraRepository

interface TeamRepository : CassandraRepository<TeamEntity, String> {

    fun findByKeySeason(season: String) : List<TeamEntity>

    fun findByKeySeasonAndKeyTeamCode(season: String, teamCode: String): TeamEntity?

}