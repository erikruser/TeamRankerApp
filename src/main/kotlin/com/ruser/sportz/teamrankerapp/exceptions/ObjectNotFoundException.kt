package com.ruser.sportz.teamrankerapp.exceptions

class ObjectNotFoundException(val responseObject: ResponseObject) : Throwable()

data class ResponseObject(
    val message: String,
    val objectType: String,
    val objectId: String
)