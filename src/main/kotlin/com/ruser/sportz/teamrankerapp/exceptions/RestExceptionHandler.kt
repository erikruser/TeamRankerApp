package com.ruser.sportz.teamrankerapp.exceptions

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(ObjectNotFoundException::class)
    fun handleObjectNotFound(objectNotFoundException: ObjectNotFoundException) : ResponseEntity<ResponseObject> {
        return ResponseEntity(objectNotFoundException.responseObject, HttpStatus.NOT_FOUND)
    }

}