package com.ruser.sportz.teamrankerapp.controller

import com.ruser.sportz.teamrankerapp.model.TeamDto
import com.ruser.sportz.teamrankerapp.service.RankerService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("team-ranker")
@CrossOrigin
class TeamController(
    private val rankerService: RankerService
) {

    @GetMapping("{season}/teams")
    fun getAllTeams(
        @PathVariable season: String,
        @RequestParam sortAlpha: Boolean
    ): ResponseEntity<List<TeamDto?>> {
        return ResponseEntity(rankerService.getAllTeams(season, sortAlpha), HttpStatus.OK)
    }

    @GetMapping("{season}/teams/{id}")
    fun getTeam(
        @PathVariable season: String,
        @PathVariable id: String
    ): ResponseEntity<TeamDto> {
        return ResponseEntity(rankerService.getTeam(season, id), HttpStatus.OK)
    }

    @PostMapping("{season}/teams/")
    fun addNewTeam(
        @PathVariable season: String,
        @RequestBody teamDto: TeamDto
    ): ResponseEntity<TeamDto> {
        rankerService.addTeam(season, teamDto)
        return ResponseEntity(rankerService.getTeam(season, teamDto.teamCode), HttpStatus.CREATED)
    }

    @GetMapping("{season}/teams/report")
    fun getTeamsReport(
        @PathVariable season: String
    ): String {
        return rankerService.getReport(season)
    }

}