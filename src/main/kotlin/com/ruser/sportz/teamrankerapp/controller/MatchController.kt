package com.ruser.sportz.teamrankerapp.controller

import com.ruser.sportz.teamrankerapp.model.MatchDto
import com.ruser.sportz.teamrankerapp.service.RankerService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("team-ranker")
@CrossOrigin
class MatchController (
    private val rankerService: RankerService
){

    @GetMapping("{season}/matches")
    fun getAllMatches(
        @PathVariable season: String
    ): ResponseEntity<List<MatchDto>> {
        return ResponseEntity(rankerService.getAllMatches(season), HttpStatus.OK)
    }

    @GetMapping("{season}/matches/{matchId}")
    fun getMatch(
        @PathVariable season: String,
        @PathVariable matchId: String
    ) : ResponseEntity<MatchDto> {
        val match = rankerService.getMatch(season, UUID.fromString(matchId))
        return if(match != null){
            ResponseEntity(match, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping("{season}/matches/")
    fun addMatch(
        @PathVariable season: String,
        @RequestBody matchDto: MatchDto
    ) : ResponseEntity<MatchDto> {
        val matchId = UUID.randomUUID()
        matchDto.id = matchId
        rankerService.addMatch(season, matchDto)
        return ResponseEntity(rankerService.getMatch(season, matchDto.id!!), HttpStatus.CREATED)
    }

}