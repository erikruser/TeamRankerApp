package com.ruser.sportz.teamrankerapp.controller

import com.ruser.sportz.teamrankerapp.model.SeasonDto
import com.ruser.sportz.teamrankerapp.service.RankerService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("team-ranker")
@CrossOrigin
class SeasonController (
    private val rankerService: RankerService
){

    @GetMapping()
    fun getAllSeasons() : ResponseEntity<List<SeasonDto>> {
        return ResponseEntity(rankerService.getAllSeasons(), HttpStatus.OK)
    }

    @PostMapping("/")
    fun addSeason(
        @RequestBody seasonDto: SeasonDto
    ) : ResponseEntity<SeasonDto> {
        rankerService.addSeason(seasonDto)
        return ResponseEntity(seasonDto, HttpStatus.CREATED)
    }

}