package com.ruser.sportz.teamrankerapp.util

import com.ruser.sportz.Match
import com.ruser.sportz.Ranker
import com.ruser.sportz.Team
import com.ruser.sportz.teamrankerapp.model.*
import com.ruser.sportz.teamrankerapp.repo.TeamRepository
import org.springframework.stereotype.Component

@Component
class Mapper (
    private val teamRepository: TeamRepository,
){

    fun mapTeamDtoToEntity(season: String, teamDto: TeamDto): TeamEntity {
        val teamKey = TeamPrimaryKey(season, teamDto.teamCode)
        return TeamEntity(teamKey, teamDto.teamName)
    }

    fun mapTeamEntityToDto(team: TeamEntity?): TeamDto? {
        return if(team != null) {
            TeamDto(team.teamName, team.key.teamCode)
        }else{
            null
        }
    }

    fun mapTeamDtoToLib(teamDto: TeamDto): Team {
        return Team(teamDto.teamCode, teamDto.teamName)
    }

    fun mapTeamLibToDto(team: Team): TeamDto {
        val teamDto = TeamDto(team.name, team.code)
        teamDto.weightedRecord = team.rank
        teamDto.wins = team.wins
        teamDto.losses = team.losses
        return teamDto
    }

    fun mapTeamEntityToLib(team: TeamEntity): Team {
        return Team(team.key.teamCode, team.teamName)
    }

    fun mapTeamLibToEntity(season: String, team: Team): TeamEntity {
        val teamKey = TeamPrimaryKey(season, team.code)
        return TeamEntity(teamKey, team.name)
    }


    fun mapMatchDtoToEntity(season: String, matchDto: MatchDto): MatchEntity {
        val matchKey = MatchPrimaryKey(season, matchDto.id!!)
        return MatchEntity(matchKey, matchDto.winner, matchDto.loser, matchDto.group)
    }

    fun mapMatchEntityToDto(match: MatchEntity?): MatchDto? {
        return if (match != null) {
            MatchDto(match.key.id, match.winner, match.loser, match.notes)
        } else {
            null
        }
    }

    fun mapMatchEntityToLib(ranker: Ranker, match: MatchEntity): Match? {
        val winner = ranker.getTeam(match.winner)
        if(winner == null){
            //todo: logger
            println("WARNING: match ${match.key} has invalid winner code: ${match.winner}")
            return null
        }
        val loser = ranker.getTeam(match.loser)
        if(loser == null){
            //todo: logger
            println("WARNING: match ${match.key} has invalid loser code: ${match.loser}")
            return null
        }
        return Match(winner, loser)
    }

    fun mapMatchDtoToLib(ranker: Ranker, matchDto: MatchDto): Match? {
        val winner = ranker.getTeam(matchDto.winner)
        val loser = ranker.getTeam(matchDto.loser)
        return Match(winner, loser)
    }

    fun mapSeasonDtoToEntity(s: SeasonDto): SeasonEntity {
        return SeasonEntity(s.seasonName, s.description)
    }

    fun mapSeasonEntityToDto(s: SeasonEntity): SeasonDto {
        return SeasonDto(s.season, s.description)
    }

}